# Local Setup

In this docs we will use the following technologies for setup:

- [Tilt](https://tilt.dev/)
- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [Helm](https://helm.sh/)
- [mkcert](https://mkcert.org/)

## Step 1: Setup minikube or OrbStack

Please follow either:

1. [documentation minikube.sigs.k8s.io](https://minikube.sigs.k8s.io/docs/start/).
2. [documentation OrbStack](https://docs.orbstack.dev/quick-start)

## Step 2: Setup Tilt

Please follow [documentation](https://docs.tilt.dev/install.html).

## Step 3: Configure minikube - Tilt

Please follow [documentation](https://docs.tilt.dev/choosing_clusters.html#minikube).

## Step 4: install mkcert

Please follow [documentation](https://github.com/FiloSottile/mkcert).

After installation, do not forget to run:

    $ mkcert -install

## Step 5: Add secret:

    $ kubectl create namespace cert-manager
    $ kubectl -n cert-manager create secret tls mkcert \
        --key "$(mkcert -CAROOT)/rootCA-key.pem" \
        --cert "$(mkcert -CAROOT)/rootCA.pem"

## Step 6: ingress

### Minikube

Enable ingress:

    $ minikube addons enable ingress

Configure ingress (we created the secret `cert-manager/mkcert` in the last step).

    $ minikube addons configure ingress

Now turn it off and on again:

    $ minikube addons disable ingress
    $ minikube addons enable

### OrbStack

[Follow documentation](https://docs.orbstack.dev/kubernetes/#loadbalancer-ingress)

## Step 7: Install CRDs and cert-manager

Please follow [documentation](https://cert-manager.io/docs/installation/helm/#4-install-cert-manager). Make sure CRDs are also installed using either of these options in the [documentation](https://cert-manager.io/docs/installation/helm/#3-install-customresourcedefinitions).

## Step 8: Create a Cluster Issuer

    $ kubectl apply -f local-setup/cluster-issuer.yaml

## Step 9: LoadBalancer

[See documentation](https://metallb.universe.tf/installation/)

## Step 10: DNS / resolving hostnames

#### Option OrbStack

See [documentation](https://docs.orbstack.dev/kubernetes/#loadbalancer-ingress)

### minikube

#### Option A. dnsmasq

Please follow [documentation for Ubuntu](https://computingforgeeks.com/install-and-configure-dnsmasq-on-ubuntu/) or [documentation for macOS](https://gist.github.com/ogrrd/5831371).

After installation, please run:

    $ echo 'address=/nldoc.k8s/127.0.0.1' >> "$(brew --prefix)/etc/dnsmasq.conf"

#### Option B. `/etc/hosts`

Edit `/etc/hosts/` and add new lines:

    127.0.0.1 publicatietool.nldoc.k8s
    127.0.0.1 rabbitmq.nldoc.k8s
    127.0.0.1 minio.nldoc.k8s
    127.0.0.1 esc.nldoc.k8s

#### Option C. Ingress DNS

I haven't tried this but it's surely possible, please see [documentation](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/).

## Step 11: `minikube tunnel` (for minikube)

Now run:

    $ minikube tunnel

The services will be exposed to the correct ports.

## After

After, you can run some commands like:

    $ minikube addons enable metrics-server
    $ minikube dashboard

This enables and starts a dashboard, including metrics about your pods.
