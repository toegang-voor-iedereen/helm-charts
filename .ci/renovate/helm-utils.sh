#!/bin/bash

source semver-utils.sh

function green() { echo -e "\033[32m$1\033[0m"; }
function red() { echo -e "\033[31m$1\033[0m"; }

# Get the version from a Helm Chart.yaml file.
# Usage:
#   ```
#   get_chart_version "helm/nldoc-conversion"
#   ```
function get_chart_version() {
  local chart_file="$1/Chart.yaml"
  yq ".version" "$chart_file"
}

# Set the version in a Helm Chart.yaml file.
# Usage:
#   ```
#   set_chart_version "helm/nldoc-conversion" "1.2.3"
#   ```
function set_chart_version() {
  local chart_file="$1/Chart.yaml"
  local new_version="$2"

  yq -P -i ".version = \"$new_version\"" "$chart_file"
}

# Get the version of a dependency in a Helm Chart.lock file.
# Usage:
#   ```
#   get_chart_lock_version "helm/nldoc-conversion" "element-structure-converter-web"
#   ```
function get_chart_lock_version() {
  local chart_file="$1/Chart.lock"
  local dep_name="$2"

  yq ".dependencies[] | select(.name == \"$dep_name\") | .version" "$chart_file" | head -n 1
}

# Set the version of a dependency in a Helm Chart.lock file.
# Usage:
#   ```
#   set_chart_lock_version "helm/nldoc-conversion" "element-structure-converter-web" "1.2.3"
#   ```
function set_chart_lock_version() {
  local chart_lock="$1/Chart.lock"
  local dep_name="$2"
  local new_version="$3"

  yq -P -i ".dependencies[] |= (select(.name == \"$dep_name\").version = \"$new_version\")" "$chart_lock"
}

# Get the version of a dependency in a Helm Chart.yaml file.
# Usage:
#   ```
#   get_chart_dep_version "helm/nldoc-conversion" "element-structure-converter-web"
#   ```
function get_chart_dep_version() {
  local chart_file="$1/Chart.yaml"
  local dep_name="$2"

  yq ".dependencies[] | select(.name == \"$dep_name\") | .version" "$chart_file" | head -n 1
}

# Set the version of a dependency in a Helm Chart.yaml file.
# Usage:
#   ```
#   set_chart_dep_version "helm/nldoc-conversion" "element-structure-converter-web" "1.2.3"
#   ```
function set_chart_dep_version() {
  local chart_file="$1/Chart.yaml"
  local dep_name="$2"
  local new_version="$3"

  yq -P -i ".dependencies[] |= (select(.name == \"$dep_name\").version = \"$new_version\")" "$chart_file"
}

# Get the aliases of a dependency in a Helm Chart.yaml file.
# Usage:
#   ```
#   get_chart_dep_aliases "helm/nldoc-conversion" "element-structure-converter-web"
#   ```
# Note: The output is a list of aliases, one per line.
function get_chart_dep_aliases() {
  local chart_file="$1/Chart.yaml"
  local dep_name="$2"

  yq ".dependencies[] | select(.name == \"$dep_name\") | .alias" "$chart_file"
}

# Get the alias of a dependency in a Helm Chart.yaml file.
# Usage:
#   ```
#   get_chart_dep_alias "helm/nldoc-conversion" "element-structure-converter-web"
#   ```
# Note: unlike get_chart_dep_aliases, this function returns only the first alias.
function get_chart_dep_alias() {
  get_chart_dep_aliases "$@" | head -n 1
}

#--------------------------------------------------------------------------------------------------

# Get the image name for a service.
# Usage:
#   ```
#   get_service_image "element-structure-converter-web"
#   ```
function get_service_image() {
  local service_name="$1"
  case "$service_name" in
    "element-structure-converter-web")
      echo "registry.gitlab.com/toegang-voor-iedereen/elementstructureconverter"
      ;;
    "element-structure-converter-amqp")
      echo "registry.gitlab.com/toegang-voor-iedereen/elementstructureconverter"
      ;;
    "nldoc-p4")
      echo "registry.gitlab.com/toegang-voor-iedereen/conversion/preprocessing/p4"
      ;;
    "publicatietool")
      echo "registry.gitlab.com/toegang-voor-iedereen/publicatietool-poc"
      ;;
    *)
      echo ">>> ERROR: Unknown service_name: $service_name"
      exit 1
      ;;
  esac
}

# Executes a given yq command for each test in the chart's test suite.
# Usage example:
#   ```
#   foreach_test_do_yq "helm/nldoc-conversion" '
#     with(select(.metadata.name | endswith("web-esc")) | .metadata.labels ; ."app.kubernetes.io/version" = "1.2.3")
#   '
#   ```
function foreach_test_do_yq() {
  local chart_folder="$1"
  local yq_cmd="$2"
  local tests_folder="${3:-tests/$chart_folder}"

  for test_file in "$tests_folder"/**/expected.yaml; do
    yq -P -i "$yq_cmd" "$test_file"
  done
}

# Updates the tests for the element-structure-converter-web dependency of nldoc-conversion
function update_esc_web_tests() {
  local chart_folder="$1"
  local new_version="$2"
  local tests_folder="${3:-tests/$chart_folder}" # optional, used for testing

  local dependency_name="element-structure-converter-web"
  local dep_alias="web-esc"

  export NEXT_VERSION="$new_version"
  export CHART_LABEL="$dep_alias-$new_version"
  # shellcheck disable=SC2155
  export FULL_IMAGE="$(get_service_image "$dependency_name"):$new_version"
  # There's a star here as a wildcard to match .metadata.name with yq in the expected yaml files
  export META_NAME="*$dep_alias"

  foreach_test_do_yq "$chart_folder" '
    select(.metadata.name == strenv(META_NAME)).metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.name == strenv(META_NAME)).metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.spec.containers[].image = strenv(FULL_IMAGE)
  ' "$tests_folder"
}

# Updates the tests for the element-structure-converter-amqp dependency of nldoc-conversion
function update_esc_amqp_tests() {
  local chart_folder="$1"
  local new_version="$2"
  local tests_folder="${3:-tests/$chart_folder}" # optional, used for testing

  local dependency_name="element-structure-converter-amqp"
  local dep_alias="amqp-esc"

  export NEXT_VERSION="$new_version"
  # shellcheck disable=SC2155
  export FULL_IMAGE="$(get_service_image "$dependency_name"):$new_version"

  export CHART_LABEL="$dep_alias-$new_version"
  # There's a star here as a wildcard to match .metadata.name with yq in the expected yaml files
  export META_NAME="*$dep_alias"

  foreach_test_do_yq "$chart_folder" '
    select(.metadata.name == strenv(META_NAME)).metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.name == strenv(META_NAME)).metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.spec.containers[].image = strenv(FULL_IMAGE)
  ' "$tests_folder"
}

# Updates the tests for the nldoc-p4 dependency of nldoc-conversion
function update_p4_tests() {
  local chart_folder="$1"
  local new_version="$2"
  local tests_folder="${3:-tests/$chart_folder}" # optional, used for testing

  local dependency_name="nldoc-p4"
  local dep_alias="amqp-p4"

  export NEXT_VERSION="$new_version"
  export CHART_LABEL="$dep_alias-$new_version"
  # shellcheck disable=SC2155
  export FULL_IMAGE="$(get_service_image "$dependency_name"):$new_version"
  # There's a star here as a wildcard to match .metadata.name with yq in the expected yaml files
  export META_NAME="*$dep_alias"

  foreach_test_do_yq "$chart_folder" '
    select(.metadata.name == strenv(META_NAME)).metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.name == strenv(META_NAME)).metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.name == strenv(META_NAME) and .kind == "Deployment").spec.template.spec.containers[].image = strenv(FULL_IMAGE)
  ' "$tests_folder"
}

# Updates the tests for the publicatietool dependency of nldoc
function update_publicatietool_tests() {
  local chart_folder="$1"
  local new_version="$2"
  local tests_folder="${3:-tests/$chart_folder}" # optional, used for testing

  local dependency_name="publicatietool"

  export NEXT_VERSION="$new_version"
  export META_LABEL_NAME="$dependency_name"
  export CHART_LABEL="$dependency_name-$new_version"
  # shellcheck disable=SC2155
  local service_image=$(get_service_image "$dependency_name")
  export FULL_IMAGE_NGINX="$service_image/nginx:$new_version"
  export FULL_IMAGE_PHP="$service_image/php:$new_version"

  foreach_test_do_yq "$chart_folder" '
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME)).metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME)).metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME) and (.kind == "Deployment" or .kind == "Job")).spec.template.metadata.labels."app.kubernetes.io/version" = strenv(NEXT_VERSION) |
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME) and (.kind == "Deployment" or .kind == "Job")).spec.template.metadata.labels."helm.sh/chart" = strenv(CHART_LABEL) |
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME) and .kind == "Deployment" and .metadata.name | test("nginx$")).spec.template.spec.containers[0].image = strenv(FULL_IMAGE_NGINX) |
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME) and .kind == "Deployment" and .metadata.name | test("nginx$") | not).spec.template.spec.containers[0].image = strenv(FULL_IMAGE_PHP) |
    select(.metadata.labels."app.kubernetes.io/name" == strenv(META_LABEL_NAME) and .kind == "Job").spec.template.spec.containers[0].image = strenv(FULL_IMAGE_PHP)
  ' "$tests_folder"
}

function update_tests() {
  case "$1" in
    "element-structure-converter-web")
      update_esc_web_tests "$2" "$3" "$4"
      ;;
    "element-structure-converter-amqp")
      update_esc_amqp_tests "$2" "$3" "$4"
      ;;
    "nldoc-p4")
      update_p4_tests "$2" "$3" "$4"
      ;;
    "publicatietool")
      update_publicatietool_tests "$2" "$3" "$4"
      ;;
    *)
      red ">>> ERROR: Unknown dependency_name: $1"
      exit 1
      ;;
  esac
}

# Upgrade a dependency in a Helm chart.
# The chart will also be updated with the same type of semantic version upgrade.
# The snapshot tests will also be updated to pass for the dependency's new version.
# Usage:
#   ```
#   upgrade_chart_dependency "helm/nldoc-conversion" "element-structure-converter-web" "1.2.3" "1.3.0"
#   ```
function upgrade_chart_dependency() {
  local chart_folder="$1"
  local dependency_name="$2"
  local current_version="$3"
  local new_version="$4"
  local tests_folder="$5" # optional, used for testing

  local tests_folder_str=""
  [[ "$tests_folder" != "" ]] && tests_folder_str="($tests_folder)"
  echo "$chart_folder: $dependency_name $current_version -> $new_version $tests_folder_str"

  # update the Chart.yaml and Chart.lock
  set_chart_dep_version "$chart_folder" "$dependency_name" "$new_version"
  set_chart_lock_version "$chart_folder" "$dependency_name" "$new_version"

  # the umbrella chart will get the same type of semantic version upgrade as the dependency
  local upgrade_type=
  upgrade_type="$(parse_semver_upgrade_type "$current_version" "$new_version")"

  # bump the Chart.yaml version
  local current_umbrella_version
  local new_umbrella_version
  current_umbrella_version=$(get_chart_version "$chart_folder")
  new_umbrella_version=$(upgrade_semver "$current_umbrella_version" "$upgrade_type")
  set_chart_version "$chart_folder" "$new_umbrella_version"

  # update the snapshot tests for all deployable documents from the dependency
  [[ "$tests_folder" != "--skip-tests" ]] && update_tests "$dependency_name" "$chart_folder" "$new_version" "$tests_folder"
}