#!/bin/bash
# This script is meant to be run by Renovate when it detects an update to a dependency 
# of the helm/nldoc and helm/nldoc-conversion umbrella charts.
#
# You can also run it yourself:
#  .ci/renovate/upgrade-dependency.sh <dependency_name> <current_version> <new_version>
#
# Example:
#  .ci/renovate/upgrade-dependency.sh element-structure-converter-web 5.12.3 5.12.6
#
# This script will:
#  0. Install yq to ~/.local/bin/yq if it needs to.
#  1. Update the dependency version in the umbrella chart's Chart.yaml and Chart.lock
#  2. Update the dependency version in the umbrella chart's snapshot tests
#  3. Bump the umbrella chart's version so it gets the same type of semantic version upgrade as the dependency.
#  4. If the dependency is a dependency of the nldoc-conversion umbrella chart, update the nldoc chart with this new version of nldoc-conversion as well.

if ! command -v yq &> /dev/null; then
  echo ">>> INSTALLING: yq"
  echo
  arch=$(uname -m)
  if [ "$arch" == "x86_64" ]; then
    yq_arch="amd64"
  elif [ "$arch" == "aarch64" ]; then
    yq_arch="arm64"
  else
    echo "Error: unsupported architecture for installing yq: $arch"
    exit 1
  fi
  install_to=~/.local/bin
  mkdir -p $install_to
  curl -L https://github.com/mikefarah/yq/releases/latest/download/yq_linux_$yq_arch -o $install_to/yq && chmod +x $install_to/yq
  echo ">>> INSTALLED: yq"
fi

cd "$(dirname "$0")" || exit 1

source semver-utils.sh
source helm-utils.sh

cd ../../ || exit 1

function upgrade_nldoc_dependency() {
  local dependency_name="$1"
  local current_version="$2"
  local new_version="$3"

  upgrade_chart_dependency "helm/nldoc" "$dependency_name" "$current_version" "$new_version"
}

function upgrade_nldoc_conversion_dependency() {
  local dependency_name="$1"
  local current_version="$2"
  local new_version="$3"

  local current_conversion_version
  current_conversion_version="$(get_chart_version "helm/nldoc-conversion")"

  upgrade_chart_dependency "helm/nldoc-conversion" "$dependency_name" "$current_version" "$new_version"

  local new_conversion_version
  new_conversion_version="$(get_chart_version "helm/nldoc-conversion")"
  
  # after updating nldoc-conversion we also update the nldoc umbrella chart that contains nldoc-conversion.
  upgrade_chart_dependency "helm/nldoc" "nldoc-conversion" "$current_conversion_version" "$new_conversion_version" --skip-tests
  update_tests "$dependency_name" "helm/nldoc" "$new_version"
}

function main() {
  local dep_name="$1"
  local current_version="$2"
  local new_version="$3"

  case "$dep_name" in
    "element-structure-converter-web")
      upgrade_nldoc_conversion_dependency "$dep_name" "$current_version" "$new_version"
      ;;
    "element-structure-converter-amqp")
      upgrade_nldoc_conversion_dependency "$dep_name" "$current_version" "$new_version"
      ;;
    "nldoc-p4")
      upgrade_nldoc_conversion_dependency "$dep_name" "$current_version" "$new_version"
      ;;
    "publicatietool")
      upgrade_nldoc_dependency "$dep_name" "$current_version" "$new_version"
      ;;
    *)
      echo ">>> SKIPPING: Nothing to do for $dep_name"
      ;;
  esac
}

main "$@"
