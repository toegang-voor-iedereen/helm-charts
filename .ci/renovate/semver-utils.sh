#!/bin/bash

# Trim leading and trailing whitespace characters.
# Usage:
#   ```
#   echo_trimmed "  hello world  " # == "hello world"
#   ```
function echo_trimmed() {
  local var="$1"
  var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
  var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
  echo -n "$var"
}

# Parse semantic version strings.
# Usage: 
#   ```
#   read -r -a semver <<< "$(parse_semver '1.2.3')"
#   major=${semver[0]}
#   minor=${semver[1]}
#   patch=${semver[2]}
#   ```
# NOTE: sourced from https://gist.github.com/jlinoff/9211310b738e83c4a2bbe14876cd2e55
function parse_semver() {
  local token="$1"
  local major=0
  local minor=0
  local patch=0

  if grep -E '^[0-9]+\.[0-9]+\.[0-9]+' <<<"$token" >/dev/null 2>&1 ; then
    # It has the correct syntax.
    local n=${token//[!0-9]/ }
    local a=("${n//\./ }")
    major=${a[0]}
    minor=${a[1]}
    patch=${a[2]}
  fi
  
  echo_trimmed "$major $minor $patch"
}

# Parse semantic version strings and return the upgrade type.
# Usage:
#   ```
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "1.2.4") # == "patch"
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "1.2.9") # == "patch"
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "1.3.0") # == "minor"
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "1.9.3") # == "minor"
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "2.0.0") # == "major"
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "2.2.3") # == "major"
#   upgrade_type=$(parse_semver_upgrade_type "1.2.3" "1.2.3") # == "none"
#   ```
function parse_semver_upgrade_type() {
  local current_version="$1"
  local new_version="$2"

  read -r -a current_semver <<< "$(parse_semver "$current_version")"
  read -r -a new_semver <<< "$(parse_semver "$new_version")"

  if [[ ${new_semver[0]} -gt ${current_semver[0]} ]]; then
    echo "major"
  elif [[ ${new_semver[1]} -gt ${current_semver[1]} ]]; then
    echo "minor"
  elif [[ ${new_semver[2]} -gt ${current_semver[2]} ]]; then
    echo "patch"
  else
    echo "none"
  fi
}

# Upgrade semantic version strings with the given upgrade type.
# Usage:
#   ```
#   upgrade_semver "1.2.3" "major" # == "2.0.0"
#   upgrade_semver "1.2.3" "minor" # == "1.3.0"
#   upgrade_semver "1.2.3" "patch" # == "1.2.4"
#   ```
function upgrade_semver() {
  local current_version="$1"
  local upgrade_type="$2"

  read -r -a current_semver <<< "$(parse_semver "$current_version")"
  local major=${current_semver[0]}
  local minor=${current_semver[1]}
  local patch=${current_semver[2]}

  case "$upgrade_type" in
    "major")
      major=$((major + 1))
      minor=0
      patch=0
      ;;
    "minor")
      minor=$((minor + 1))
      patch=0
      ;;
    "patch")
      patch=$((patch + 1))
      ;;
    "none")
      ;;
    *)
      echo ">>> ERROR: Unknown upgrade_type: $upgrade_type"
      ;;
  esac

  echo "$major.$minor.$patch"
}
