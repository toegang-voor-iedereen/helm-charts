#!/bin/bash

function green() { echo -e "\033[32m$1\033[0m"; }
function red() { echo -e "\033[31m$1\033[0m"; }

function assert_equals() {
  local expected="$1"
  local actual="$2"
  if [[ "$expected" != "$actual" ]]; then
    echo "$(red FAIL): Expected '$expected' but got '$actual'"
    exit 1
  fi
}

function run_test() {
  echo -n "> Running test: " "$@"
  echo
  if "$@"; then
    green PASS
  else
    red FAIL
    exit 1
  fi
}
