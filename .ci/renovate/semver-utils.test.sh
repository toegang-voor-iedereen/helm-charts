#!/bin/bash

cd "$(dirname "$0")" || exit 1

source semver-utils.sh
source test-utils.sh

function test_parse_semver() {
  read -r -a semver <<< "$(parse_semver '1.2.3')"
  local major="${semver[0]}"
  local minor="${semver[1]}"
  local patch="${semver[2]}"
  assert_equals "1" "$major"
  assert_equals "2" "$minor"
  assert_equals "3" "$patch"

  assert_equals "1 2 3" "$(parse_semver "1.2.3")"
  assert_equals "1 2 4" "$(parse_semver "1.2.4+foobar")"
  assert_equals "1 2 5      01" "$(parse_semver "1.2.5-alpha01")"
  assert_equals "0 0 0" "$(parse_semver "not-a-semver")"
  assert_equals "2 3 4" "$(parse_semver "2.3.4abc")"
  assert_equals "0 0 0" "$(parse_semver "a1.2.3")"
}

function test_parse_semver_upgrade_type() {
  assert_equals "patch" "$(parse_semver_upgrade_type "1.2.3" "1.2.4")"
  assert_equals "patch" "$(parse_semver_upgrade_type "1.2.3" "1.2.9")"
  assert_equals "minor" "$(parse_semver_upgrade_type "1.2.3" "1.3.0")"
  assert_equals "minor" "$(parse_semver_upgrade_type "1.2.3" "1.9.3")"
  assert_equals "major" "$(parse_semver_upgrade_type "1.2.3" "2.0.0")"
  assert_equals "major" "$(parse_semver_upgrade_type "1.2.3" "2.2.3")"
  assert_equals "none" "$(parse_semver_upgrade_type "1.2.3" "1.2.3")"
}

function test_upgrade_semver() {
  assert_equals "2.0.0" "$(upgrade_semver "1.2.3" "major")"
  assert_equals "1.3.0" "$(upgrade_semver "1.2.3" "minor")"
  assert_equals "1.2.4" "$(upgrade_semver "1.2.3" "patch")"
  assert_equals "1.2.3" "$(upgrade_semver "1.2.3" "none")"
}

function run() {
  run_test test_parse_semver
  run_test test_parse_semver_upgrade_type
  run_test test_upgrade_semver
}

run
