#!/bin/bash -e

cd "$(dirname "$0")"

source helm-utils.sh
source test-utils.sh

cd ../../

function test_get_set_chart_version() {
  local test_folder="/tmp/helm-utils.test/get_set_chart_version"
  mkdir -p $test_folder
  cp helm/nldoc-conversion/Chart.yaml $test_folder

  local new_version="1.2.3"

  set_chart_version "$test_folder" "$new_version"

  local actual
  actual=$(get_chart_version "$test_folder")
  assert_equals "$new_version" "$actual"
}

function test_set_chart_lock_version() {
  local test_folder="/tmp/helm-utils.test/set_chart_lock_version"
  mkdir -p $test_folder
  cp helm/nldoc-conversion/Chart.lock $test_folder

  local dep_name="element-structure-converter-web"
  local new_version="1.2.3"

  set_chart_lock_version "$test_folder" "$dep_name" "$new_version"

  local actual
  actual=$(yq ".dependencies[] | select(.name == \"$dep_name\") | .version" "$test_folder/Chart.lock")
  assert_equals "$new_version" "$actual"
}

function test_set_chart_dep_version() {
  local test_folder="/tmp/helm-utils.test/set_chart_dep_version"
  mkdir -p $test_folder
  cp helm/nldoc-conversion/Chart.yaml $test_folder

  local dep_name="element-structure-converter-web"
  local new_version="1.2.3"

  set_chart_dep_version "$test_folder" "$dep_name" "$new_version"

  local actual
  actual=$(yq ".dependencies[] | select(.name == \"$dep_name\") | .version" "$test_folder/Chart.yaml")
  assert_equals "$new_version" "$actual"
}

function test_get_chart_dep_alias() {
  local test_folder="/tmp/helm-utils.test/get_chart_dep_alias"
  mkdir -p $test_folder
  cp helm/nldoc-conversion/Chart.yaml $test_folder

  local dep_name="element-structure-converter-web"

  local actual
  actual=$(get_chart_dep_alias "$test_folder" "$dep_name")
  assert_equals "web-esc" "$actual"

  local dep_name="element-structure-converter-amqp"
  actual=$(get_chart_dep_alias "$test_folder" "$dep_name")
  assert_equals "amqp-esc" "$actual"
}

function test_upgrade_chart_dependency() {
  local umbrella_chart="$1"
  local dep_name="$2"
  local current_version="$3"
  local new_version="$4"
  local new_umbrella_version="${5:-0.0.1}"

  # Given a Helm chart folder.
  local test_folder="/tmp/helm-utils.test/upgrade_chart_dependency/$dep_name"
  rm -rf "$test_folder" && mkdir -p "$test_folder"
  cp -r "$umbrella_chart"/* "$test_folder"
  [[ "$umbrella_chart" == "helm/nldoc" ]] && cp -r helm/nldoc-conversion "$test_folder/../nldoc-conversion"
  set_chart_version "$test_folder" "0.0.0"

  # ... for which the Helm chart tests pass,
  set +e
  local output
  if ! output=$(tests/helm/run.sh "$test_folder" "tests/$umbrella_chart"); then
    echo
    red "Error verifying pre-condition!"
    red "Expecting Helm chart tests for $umbrella_chart to pass, but got:"
    echo
    echo "$output"
    exit 1
  fi
  set -e

  # When upgrading a dependency version in that chart,
  upgrade_chart_dependency "$test_folder" "$dep_name" "$current_version" "$new_version" "tests/$umbrella_chart"

  # Then after a helm dependency update, the Helm chart tests should still pass.
  helm dependency update "$test_folder" > /dev/null

  set +e
  local output
  if ! output=$(tests/helm/run.sh "$test_folder" "tests/$umbrella_chart"); then
    echo
    red "Expecting Helm chart tests for $umbrella_chart to pass, but got:"
    echo
    echo "$output"
    exit 1
  fi
  set -e

  # and the dependency version should be updated in the Chart.yaml and Chart.lock
  assert_equals "$new_version" "$(get_chart_dep_version "$test_folder" "$dep_name")"
  assert_equals "$new_version" "$(get_chart_lock_version "$test_folder" "$dep_name")"

  # and the umbrella chart version should be bumped
  assert_equals "$new_umbrella_version" "$(get_chart_version "$test_folder")"

  ## Cleanup
  git restore "tests/$umbrella_chart/**/expected.yaml"
}

function run() {
  run_test test_get_set_chart_version
  run_test test_set_chart_lock_version
  run_test test_set_chart_dep_version
  run_test test_get_chart_dep_alias
  ## NOTE: the current and new versions must be actual existing versions in the GitLab chart repo.
  run_test test_upgrade_chart_dependency helm/nldoc-conversion element-structure-converter-web "6.0.0" "6.0.0" "0.0.0" # TODO: update to 6.0.1 after that is released
  run_test test_upgrade_chart_dependency helm/nldoc-conversion element-structure-converter-amqp "6.0.0" "6.0.0" "0.0.0"
  run_test test_upgrade_chart_dependency helm/nldoc-conversion nldoc-p4 "0.8.7" "0.8.8"
  run_test test_upgrade_chart_dependency helm/nldoc publicatietool "3.40.2" "3.40.3"
}

run
