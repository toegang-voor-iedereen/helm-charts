#!/bin/bash

# Check if the helm directory exists
if [ ! -d "./helm" ]; then
  echo "Directory ./helm does not exist."
  exit 1
fi

# shellcheck disable=SC2038
find ./helm -name "Chart.lock" | xargs rm

# Iterate over each subdirectory and run `helm dependency update`
for subdir in ./helm/* ; do
  if [ -d "$subdir" ]; then
    helm dependency update "$subdir"
  fi
done
