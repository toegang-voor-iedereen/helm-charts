# Helm Charts

This is a monorepo for the Helm charts that we at NLdoc manage for our product. The Helm chart for each microservice is included in each service's respective repository, so this repo primarily contains umbrella charts.

## TODO

- Correctly configure Renovate to automatically update the versions in the tests and bump the umbrella chart version.
- enable Renovate automerge of patch (and maybe even minor?) versions.
