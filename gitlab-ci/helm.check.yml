spec:
  inputs:
    folders:
      type: array
      description: 'Optional: the folders relative to the root of the repository in which there are Helm chart files are stored. Defaults to `helm`'
      default:
        - helm

    tests:
      type: string
      description: 'Optional: the command to run tests for the given Helm chart. Defaults to `test/helm/run.sh`'
      default: test/helm/run.sh

    testsDir:
      type: string
      description: 'Optional: the directory in the repository that contains all the Helm chart tests. The helm:check job will run on changes to this folder. Defaults to `test/helm`'
      default: test/helm

    extraRules:
      type: array
      description: 'Optional: specify some extra `rules` when this job should run. Use this if you e.g. also want to run this job on e.g. all commits to master'
      default: []

---

helm:check:
  stage: test
  image: registry.gitlab.com/toegang-voor-iedereen/docker-images/docker/docker-ci-cd:latest
  variables:
    FORCE_COLOR: true
  tags: ["arm64"] # temporarily run on ARM64 runner because the AMD runner is constantly running into TLS errors.
  before_script:
    - red() { echo -e "\033[31m$1\033[0m"; }
    - bold() { echo -e "\033[1m$1\033[0m"; }
    - yellow() { echo -e "\033[33m$1\033[0m"; }
  script: 
    - |
      for folder in $(echo "$[[ inputs.folders ]]" | yq '.[]'); do
        [[ ! -d "$folder" ]] && red "Error: $folder does not exist or is not a folder" && exit 1
        [[ ! -f "$folder/Chart.yaml" ]] && red "Error: $folder does not contain a Chart.yaml" && exit 1
        [[ ! -f "$folder/Chart.lock" ]] && yellow "Warning: $folder does not contain a Chart.lock. Please run 'helm dependency update' and commit the Chart.lock file"

        echo
        bold "> Downloading Helm chart dependencies for chart in folder $folder"
        echo

        helm dependency update "./$folder"

        echo
        bold "> Performing Helm checks for chart in folder $folder"
        echo

        bold "> helm lint $folder"
        helm lint "./$folder"
        bold "> helm template $folder"
        helm template "./$folder"
        bold "> helm kubeconform $folder"
        helm kubeconform "./$folder"

        [[ -n "$[[ inputs.tests ]]" ]] && bold "> $[[ inputs.tests ]] \"$folder\"" && $[[ inputs.tests ]] "$folder"
      done
  rules:
    - if: $CI_COMMIT_TAG
    - changes:
      - $[[ inputs.folders ]]/**/*
      - $[[ inputs.testsDir ]]/**/*
    - $[[ inputs.extraRules ]]
