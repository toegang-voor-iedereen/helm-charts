# Gitlab CI job templates for Helm charts

This folder contains several Gitlab CI templates with jobs for repositories that contain Helm charts.

## `helm:check`

The `helm:check` template runs a job to test the correctness of the Helm chart in the including repository.

- By default, this job looks for a Helm chart in the `helm` folder.
- This job runs only on tags, or when there are changes to the files inside the chart folder(s) and tests folder.
- This job will also run tests if there are any. By default, it will execute `test/helm/run.sh` if it exists, but this can be overridden with `inputs.tests`.

### Usage

```yml
include:
  - project: '${CI_PROJECT_ROOT_NAMESPACE}/helm-charts'
    ref: 'main'
    file: '/gitlab-ci/helm.check.yml'
```

To override the folders in the repository that contain the Helm charts, use `inputs.folders` to specify folders relative to the root of the including repository:

```yml
include:
  - project: '${CI_PROJECT_ROOT_NAMESPACE}/helm-charts'
    ref: 'main'
    file: '/gitlab-ci/helm.check.yml'
    inputs:
      folders:
      - deployment
```

To override the command for running the tests, use `inputs.tests` to specify any valid Bash command. It will receive the current chart folder as first argument.

```yml
include:
  - project: '${CI_PROJECT_ROOT_NAMESPACE}/helm-charts'
    ref: 'main'
    file: '/gitlab-ci/helm.check.yml'
    inputs:
      tests: echo 'helm' | grep -q
# expands to `echo 'helm' | grep -q helm`
```

## `helm:publish`

The `helm:publish` template runs a job to publish the Helm chart in the including repository to the that project's Helm chart index.

- By default, this job looks for a Helm chart in the `helm` folder.
- This job runs only on tags.
- If the chart's version already exists in the project's Helm chart index, then the chart won't be pushed, but the job will still complete successfully.

### Usage

```yml
include:
  - project: '${CI_PROJECT_ROOT_NAMESPACE}/helm-charts'
    ref: 'main'
    file: '/gitlab-ci/helm.publish.yml'
```

To override the folders in the repository that contain the Helm charts, use `inputs.folders` to specify folders relative to the root of the including repository:

```yml
include:
  - project: '${CI_PROJECT_ROOT_NAMESPACE}/helm-charts'
    ref: 'main'
    file: '/gitlab-ci/helm.publish.yml'
    inputs:
      folders:
      - deployment
```
