#!/bin/bash

# cd to root of repository
cd "$(dirname "$0")/../.." || exit

SUCCESS=true

green() {
  echo -e "\033[32m$1\033[0m"
}

red() {
  echo -e "\033[31m$1\033[0m"
}

hash_yaml_file() {
  local file="$1"
  echo -n "$(yq -o json "$file" | jq -S . | sha256sum | cut -d ' ' -f 1)"
}

assert_same_yaml() {
  local expected="$1"
  local actual="$2"

  if [ "$(hash_yaml_file "$expected")" == "$(hash_yaml_file "$actual")" ]; then
    green PASS
    return 0
  else
    red FAIL
    echo
    echo "Expected $2 to have the same contents as $1, but got diff:"
    # pass through yq to ensure formatting issues (whitespace and quotes) won't show up in diffs, only changes in contents.
    diff --color <(yq -P . "$expected") <(yq -P . "$actual")
    echo
    echo "Use the following command to update the snapshot:"
    echo "cp \"$actual\" \"$expected\""
    echo
    echo
    return 1
  fi
}

run_test() {
  local chart_folder="$1"
  local test_folder="$2"

  echo "> $test_folder"

  helm template "$chart_folder" -f "$test_folder/values.yaml" > "$test_folder/actual.yaml"

  assert_same_yaml "$test_folder/expected.yaml" "$test_folder/actual.yaml"
}

run_suite() {
  local chart_folder=$1
  local suite_folder=$2

  echo
  echo "Running Helm chart test suite ..."
  echo

  for test_folder in "$suite_folder"/*; do
    if [[ -d "$test_folder" ]]; then
      if ! run_test "$chart_folder" "$test_folder"; then
        SUCCESS="false"
      fi
    fi
  done
}

main() {
  local chart_folder="$1"
  [[ -z "$chart_folder" ]] && echo "Error: expecting Helm chart folder as first argument" && exit 1
  [[ ! -d "$chart_folder" ]] && echo "Error: $chart_folder is not a directory" && exit 1

  local tests_folder="${2:-tests/$chart_folder}"
  [[ ! -d "$tests_folder" ]] && echo "Error: $tests_folder is not a directory" && exit 1

  run_suite "$chart_folder" "$tests_folder"

  echo
  if [[ $SUCCESS == "true" ]]; then
    echo "Success! All tests passed!"
  else
    echo "Oh no, there were failing tests!"
    exit 1
  fi
}

main "$@"
